using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMover : MonoBehaviour, IGAiming, IGAchering
{
    [SerializeField] private float CameraSens = 1;
    private Transform ancherObject = null;
    private Vector3 ancherPosition;
    
    private Transform cameraPos;
    // Start is called before the first frame update
    void Start()
    {
        cameraPos = GetComponentInChildren<Camera>().transform;
    }

    

    public void GAiming(Vector3 correctAngle)
    {
        transform.Rotate(-Vector3.up*(correctAngle.x*CameraSens));
        cameraPos.Rotate(Vector3.right * (correctAngle.y*CameraSens));
    }

    public void GSetAncherObject(Transform objectForAncher)
    {
        ancherObject = objectForAncher;
    }
    public void GSetAncherObject(Vector3 pointForAncher)
    {
        ancherPosition = pointForAncher;
    }
}
