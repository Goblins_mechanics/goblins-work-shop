using System;using System.Collections;
using System.Collections.Generic;
using UnityEngine;

enum TrackTypeEnum
{
    screenPoint,
    raycastPoint,
    raycastTransform
}


public class InputController : MonoBehaviour
{
    /*
     * Ввод ориентирован на мобильные и ВЕБ
     * взодные данные: экранные координаты, ЛКМ / тач, драг
     * обработка:
     * тач / клин :
     * - на обьект (если никакой не выбран) - выбор. если возможно
     * - на уже выбранном обьекте - отмена выбора
     * - тач при выбранном обьекта на другой объект - ВО.bool Action (collision){} где Return - успешность 
     * Drag:
     * - начало - если начало на обьекте с интерфейсом драг - выбрать, если нет выбрать камеру
     * - продолжение - сообщать новые координаты
     * - конец - сообщить о конце
     */
    [SerializeField] private Transform cameraTransform;
    
    
    [SerializeField] private Transform isSelected; //only for debug. after this delete {serializeField}
    [SerializeField] private float minDragDistans = 5; //min Screen Distans For Check Drag
    [SerializeField] private List<Vector2> ScreenPositions; //screen position mouse on drag time
    [SerializeField] private List<Vector3> raycastPositions; //world position mouse on drag time 
    [SerializeField] private List<Transform> transformsPositions; //tranforms mouse on drag time
    private bool isDrag = false;
    private IGAiming aimObject;
    
    // Start is called before the first frame update
    void Start()
    {
        ScreenPositions = new List<Vector2>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            if (ScreenPositions.Count == 1)
            {
                checkRaycast(); 
                if (transformsPositions[0]?.GetComponent<IGMaySelection>() != null)
                {
                    isSelected = transformsPositions[0];
                }
                else 
                {
                    if (isSelected!=null&&isSelected.GetComponent<IGMayActionTo>() != null)
                    {
                        isSelected.GetComponent<IGMayActionTo>().GActionTo(transformsPositions[0],raycastPositions[0]);
                    }
                    else
                    {
                        isSelected = cameraTransform;
                    }
                }
                aimObject = isSelected.GetComponent<IGAiming>();
            }

            ScreenPositions.Clear();
            transformsPositions.Clear();
            raycastPositions.Clear();
            
            isDrag = false;
        }
        
        if (isDrag)
        {
            OnCursorDrag();
        }
        
        
        if (Input.GetMouseButtonDown(0))
        {
            if (!isDrag) {isDrag = true;}
        }
    }

    private void OnCursorDrag()
    {
        if (ScreenPositions.Count == 0)
        {
            ScreenPositions.Add(new Vector2(Input.mousePosition.x, Input.mousePosition.y));
            checkRaycast(); 
            
        }
        else
        {
            var newPoisition = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            if (Vector2.Distance(ScreenPositions[ScreenPositions.Count-1], newPoisition)>= minDragDistans )
            {
                Vector2 deltaPos = ScreenPositions[ScreenPositions.Count - 1] - newPoisition;
                if (aimObject != null)
                {
                    aimObject.GAiming(new Vector3(deltaPos.x, deltaPos.y,0));
                }
                ScreenPositions.Add(newPoisition);
                checkRaycast(); 
            }
        }
    }

    private void checkRaycast()
    {
        Ray ray = cameraTransform.gameObject.GetComponentInChildren<Camera>().ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            raycastPositions.Add(hit.point);
            transformsPositions.Add(hit.transform);
        }
    }


}
