
using UnityEngine;

public sealed class UserInterfaceController : MonoBehaviour
{
    [SerializeField] private Transform cannonTransform;
    private IGAiming _cannonAimingInterface; 
    private IGShooting _cannonShootingInterface;
    public void CannonFire()
    {
        if (cannonTransform==null) return;
        GetCannonCanShoot();
        _cannonShootingInterface?.GFire();
    }
    public void CannonAimUp()
    {
        if (cannonTransform==null) return;
        GetCannonCanAim();
        _cannonAimingInterface?.GAiming(Vector3.up);
    }
    public void CannonAimDown()
    {
        if (cannonTransform==null) return;
        GetCannonCanAim();
        _cannonAimingInterface?.GAiming(Vector3.down);
    }
    public void CannonAimLeft()
    {
        if (cannonTransform==null) return;
        GetCannonCanAim();
        _cannonAimingInterface?.GAiming(Vector3.left);
    }
    public void CannonAimRight()
    {
        if (cannonTransform==null) return;
        GetCannonCanAim();
        _cannonAimingInterface?.GAiming(Vector3.right);
    }
    private void GetCannonCanAim()
    {
        _cannonAimingInterface ??= cannonTransform.GetComponent<IGAiming>();
    }
    private void GetCannonCanShoot()
    {
        _cannonShootingInterface ??= cannonTransform.GetComponent<IGShooting>();
    }
}
