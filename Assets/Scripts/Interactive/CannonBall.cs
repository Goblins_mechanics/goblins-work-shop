using UnityEngine;

/// <summary>
/// you can add an object with hit effect as a child of the cannonball.
/// the object with the effect must have an interface IGCollision
/// </summary>

public sealed class CannonBall : MonoBehaviour
{
    [Tooltip("how long the ball lives after collision")]
    [SerializeField] private float cannonBallMaxLifeTime = 2;
    
    private Rigidbody _thisRigidbody;
    private void Awake()
    {
        _thisRigidbody = GetComponent<Rigidbody>();
    }

    
    /// <summary>
    /// action onCollision. calculation of the oncoming velocity vector multiplied by the masses
    /// </summary>
    /// <param name="colide"></param>
    private void SelfCollide(Collider colide)
    {
        float powerOfStrike;
        if (colide.attachedRigidbody != null)
        {
            powerOfStrike = (_thisRigidbody.velocity + colide.attachedRigidbody.velocity).magnitude * _thisRigidbody.mass;
        }
        else
        {
            powerOfStrike = _thisRigidbody.velocity.magnitude * _thisRigidbody.mass;
        }
        Destroy(this.gameObject, cannonBallMaxLifeTime);
        colide.GetComponent<HPcontrol>()?.SendDamage(powerOfStrike);
        
        Debug.Log("collision with :" + colide.transform.name + "; and send " + (int)powerOfStrike + " damage");
    }

    /// <summary>
    ///  calls all this child that have the [IGCollision] interface, and send them [collisionTransform] of collision
    /// </summary>
    /// <param name="collisionTransform"></param>
    private void SendChildAboutCollision(Transform collisionTransform)
    {
        var IGC = GetComponentsInChildren<IGCollision>();
        foreach (var item in IGC)
        {
            item.IsCollision(collisionTransform);
        }
    }
    
    private void OnCollisionEnter(Collision other)
    {
        SelfCollide(other.collider);
        SendChildAboutCollision(other.transform);
        

    }
}
