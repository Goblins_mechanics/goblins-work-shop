using UnityEngine;

public sealed class CannonBallEffect : MonoBehaviour, IGCollision
{
    [SerializeField] private GameObject boomPrefabs;
    private bool _firstCollision = true;
    public void IsCollision(Transform colisionTansform)
    {
        if (_firstCollision)
        {
            _firstCollision = false;
            GameObject boom = Instantiate(boomPrefabs, null);
            boom.transform.position = this.transform.position;
            boom.name = "----====o====-----";
            Destroy(boom.gameObject, 2f);
            boomPrefabs.GetComponent<ParticleSystem>().Play();
            
        }
    }
}
