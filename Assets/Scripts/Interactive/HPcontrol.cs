using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPcontrol : MonoBehaviour
{
    [Header("presetting")] [SerializeField]
    private float maxHP;
    private float currentHP;
    private GameObject dethEffect;

    [SerializeField] private GameObject dieEffect;

    // Start is called before the first frame update
    void Start()
    {
        currentHP = maxHP;
    }

    // Update is called once per frame
    public void SendDamage(float damage)
    {
        currentHP -= damage;
        CheckHP();
    }

    private void CheckHP()
    {
        if (currentHP < 0)
        {
            dethEffect = Instantiate(dieEffect, this.transform);
            dethEffect.transform.parent = null;
            Destroy(dethEffect, 4);
            Destroy(this.gameObject);
        }
    }
    
    private void OnDestroy()
    {
       
    }

    private void OnApplicationQuit()
    {
        Destroy(dethEffect);
    }

    
}
