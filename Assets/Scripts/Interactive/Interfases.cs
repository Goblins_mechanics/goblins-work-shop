using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// !!! первичное взаимодействие
// ! выбор обьекта
internal interface IGMaySelection
{
    public Transform GIMayBeSelection();
}

// ! действие на 
internal interface IGMayActionTo
{
    public void GActionTo(Transform Object, Vector3 hitPoint);
}

internal interface IGCollision
{
    /// <summary>
    /// interface for a ....
    /// </summary>
    /// <param name="colisionTansform"> collision object</param>
    public void IsCollision(Transform colisionTansform);
}

internal interface IGAiming
{
    public void GAiming(Vector3 correctAngle);
}

internal interface IGShooting
{
    public void GFire();
}

internal interface IGAchering
{
    public void GSetAncherObject(Transform objectForAncher);
    public void GSetAncherObject(Vector3 pointForAncher);
}



internal interface IGNeedControler
{
    public void GSetController(Transform Controller);
    
}

