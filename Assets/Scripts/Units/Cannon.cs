using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class Cannon : MonoBehaviour, IGAiming, IGShooting, IGMaySelection, IGMayActionTo
{
    [Header("Object presets")]
    [SerializeField] private float powerFire;
    [SerializeField] private float coolDownTime;
    [SerializeField] private float aimAngularSpeed;
    
    [Header("component links")]
    [SerializeField] private Transform aimAxis;
    [SerializeField] private Transform ejectPoint;
    [Header("Prefabs links")]
    [SerializeField] private GameObject ammoPrefab;
    [SerializeField] private GameObject fireEffectPrefabs;
    
    
    private bool _fire;
    private Vector3 _cannonAim;
    private float _coolDawn;

    public float CoolDownTimeLeft() 
    {
        return _coolDawn;
    }

    private void Start()
    {
        fireEffectPrefabs.GetComponent<ParticleSystem>().Stop();
    }

    private void Update()
    {
        _coolDawn -= Time.deltaTime;
        if (_fire)
        {
            CannonFire();
            
        }
        aimAxis.Rotate(Vector3.right*_cannonAim.x);
        this.transform.Rotate(Vector3.up*_cannonAim.y);
        _cannonAim=Vector3.zero;
    }

    private void CannonFire()
    {
        if (_coolDawn <= 0)
        {
            GameObject boom = Instantiate(ammoPrefab, ejectPoint);
            boom.GetComponent<Rigidbody>().AddForce(ejectPoint.forward * powerFire, ForceMode.Impulse);
            boom.transform.parent = null;
            /**/
            fireEffectPrefabs.GetComponent<ParticleSystem>().Play();
            _fire = false;
            _coolDawn = coolDownTime;
        }
    }

    private void ActionUP()
    {
        _cannonAim += Vector3.left * aimAngularSpeed;
    }

    private void ActionDOWN()
    {
        _cannonAim += Vector3.right * aimAngularSpeed;
    }

    private void ActionLEFT()
    {
        _cannonAim += Vector3.down * aimAngularSpeed;
    }

    private void ActionRIGHT()
    {
        _cannonAim += Vector3.up * aimAngularSpeed;
    }

    public void GAiming(Vector3 correctAngle)
    {
        Debug.Log("GAiming => " + correctAngle);
        if (correctAngle.x > 0)
        {
            ActionRIGHT();
        } else if (correctAngle.x < 0)
        {
            ActionLEFT();
        }
        if (correctAngle.y > 0)
        {
            ActionUP();
        } else if (correctAngle.y < 0)
        {
            ActionDOWN();
        }
    }

    public void GFire()
    {
        _fire = true;
    }

    public Transform GIMayBeSelection()
    {
        return this.transform;
    }

    public void GActionTo(Transform hitObject, Vector3 hitPoint)
    {
        Debug.Log($"transform : {hitObject} ; in point {hitPoint}");
    }
}
